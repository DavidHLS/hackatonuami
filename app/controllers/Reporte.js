//slide menu 

// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

//Guarda temporalmente variables que utilizare para la creacion del reporta dependiendo que opciones seleccione el usuario.
Ti.App.Properties.setDouble("log",0.0);
Ti.App.Properties.setDouble("lat",0.0);
Ti.App.Properties.setString('lugar','uam');
Ti.App.Properties.setString('transporte','metro');
Ti.App.Properties.setString('unidad','lerma');
Ti.App.Properties.setString('evento','retraso');
Ti.App.Properties.setString('linea','1 rosa');
Ti.App.Properties.setString('estacion','uami');

//Esta funcion crea y muestra un pequenio menu con opciones relacionadas con el metro que el usuario debera seleccionar, esto
//ocurre cuando el usuario da click en icono del metro en la ventana reporte.
function clickMetro(){
	Ti.App.Properties.setString('transporte','metro');
	var pickerUnidades = Ti.UI.createPicker({
	  top:"5%",
	  borderColor:"black",
	  width:"80%",
	  height:"15%",
	  borderRadius:7,
	  backgroundColor:"black"
	});
	
	var unidades = [];
	unidades[0]=Ti.UI.createPickerRow({title:'Selecciona Unidad UAM'});
	unidades[1]=Ti.UI.createPickerRow({title:'Azcapotzalco'});
	unidades[2]=Ti.UI.createPickerRow({title:'Cuajimalpa'});
	unidades[3]=Ti.UI.createPickerRow({title:'Iztapalapa'});
	unidades[4]=Ti.UI.createPickerRow({title:'Lerma'});
	unidades[5]=Ti.UI.createPickerRow({title:'Xochimilco'});
	pickerUnidades.add(unidades);
	pickerUnidades.selectionIndicator = true;
	
	//Obtiene el valor del las opciones de la unidades
	pickerUnidades.addEventListener('change',function(e){
		var unidad = pickerUnidades.getSelectedRow(0).title;
		Ti.App.Properties.setString('unidad',unidad);
	});
	
	var pickerEventos = Ti.UI.createPicker({
	  top:"25%",
	  borderColor:"black",
	  width:"80%",
	  height:"15%",
	  borderRadius:7,
	  backgroundColor:"black"
	});
	
	var eventos = [];
	eventos[0]=Ti.UI.createPickerRow({title:'Selecciona un evento'});
	eventos[1]=Ti.UI.createPickerRow({title:'Estación cerrada'});
	eventos[2]=Ti.UI.createPickerRow({title:'Mantenimiento'});
	eventos[3]=Ti.UI.createPickerRow({title:'Retraso'});
	
	pickerEventos.add(eventos);
	pickerEventos.selectionIndicator = true;
	
	//Obtiene el valor del las opciones de eventos
	pickerEventos.addEventListener('change',function(e){
		var evento = pickerEventos.getSelectedRow(0).title;
		Ti.App.Properties.setString('evento',evento);
	});
	
	var pickerLineas = Ti.UI.createPicker({
	  top:"45%",
	  borderColor:"black",
	  width:"80%",
	  height:"15%",
	  borderRadius:7,
	  backgroundColor:"black"
	});
	
	var lineas = [];
	lineas[0]=Ti.UI.createPickerRow({title:'Selecciona la linea'});
	lineas[1]=Ti.UI.createPickerRow({title:'1 Rosa'});
	lineas[2]=Ti.UI.createPickerRow({title:'2 Azul'});
	lineas[3]=Ti.UI.createPickerRow({title:'3 Verde-olivo'});
	lineas[4]=Ti.UI.createPickerRow({title:'4 Verde-Agua '});
	lineas[5]=Ti.UI.createPickerRow({title:'5 Amarilla'});
	lineas[6]=Ti.UI.createPickerRow({title:'6 Roja'});
	lineas[7]=Ti.UI.createPickerRow({title:'7 Naranja'});
	lineas[8]=Ti.UI.createPickerRow({title:'8 Verde '});
	lineas[9]=Ti.UI.createPickerRow({title:'9 Café '});
	lineas[10]=Ti.UI.createPickerRow({title:'A Morada '});
	lineas[11]=Ti.UI.createPickerRow({title:'B Verde-Gris '});
	lineas[12]=Ti.UI.createPickerRow({title:'12 Dorada'});
	pickerLineas.add(lineas);
	pickerLineas.selectionIndicator = true;
	
	//Obtiene el valor del las opciones de las lineas
	pickerLineas.addEventListener('change',function(e){
		var linea = pickerLineas.getSelectedRow(0).title;
		Ti.App.Properties.setString('linea',linea);
	});
	
	//Cuadro de texto donde el usuario debera escribir la estacion de metro donde se encuentra.
	var tfEstacion=Ti.UI.createTextField({
		backgroundColor:"white",
		top:"65%",
		width:"80%",
		height:"15%",
		hintText:"Estación",
		hintTextColor:"gray",
		borderColor:"black",
		borderRadius:7,
		color:"black"
	});
	
	//Se agregan los componentes a la vista ya defina en el xml  y tss.
	$.v_cuerpo.removeAllChildren();
	$.v_cuerpo.add(pickerUnidades);
	$.v_cuerpo.add(pickerEventos);
	$.v_cuerpo.add(pickerLineas);
	$.v_cuerpo.add(tfEstacion);
	$.btn_metro.setBorderColor("black");
	$.btn_particular.setBorderColor("white");
	$.btn_otro.setBorderColor("white");
	
	pickerEventos.setSelectedRow(0, 0, false);
	pickerUnidades.setSelectedRow(0, 0, false);
	
	//Se obtiene el valor del campo de texto de la estacion.
	tfEstacion.addEventListener('change', function(e){
		var estacion = tfEstacion.getValue();
		Ti.App.Properties.setString('estacion', estacion);
	});
}


//Esta funcion parecida a la anterior obtiene la informacion necesario para realizar un reporte caundo te transportas en un vehiculo perticular.
function clickParticular(){
	
	var latitude;
	var longitude; 
    //Esta funcion obtiene la ubicacion del usuario, lat y long, esto siempre y caundo la app tenga los permisos de ubicacion
	Titanium.Geolocation.getCurrentPosition(function(e) {
	    if (e.error) {
	       	Ti.API.error('Error: ' + e.error);
	   	} else {
	       	latitude = e.coords.latitude;
	       	longitude = e.coords.longitude;
	       	Ti.App.Properties.setDouble("log",longitude);
	       	Ti.App.Properties.setDouble("lat",latitude);
	       	//Esta funcion transforma lat y long en la direccion como calle, delegacion, etc.. del usuario, esto para poder
	       	//ingresarlo en los reportes.
	       	Titanium.Geolocation.reverseGeocoder(latitude,longitude, function(evt) {
        	var street;
        	var city;
        	var country;
        	if (evt.success) {
            	var places = evt.places;
            	if (places && places.length) {
                	street = places[0].street;
                	city = places[0].city;
                	country = places[0].country_code;
                	var dir = street+" "+city+" "+country;
                	Ti.App.Properties.setString('lugar',dir);
                	console.log(dir);
            	} else {
                	address = "No se encontro la direccion";
            	}
        }
    });                        
	   }
	   
	});
		
	//*********************************************************************
	//Aqui se crean los pequenios menus con las opciones a seleccionar para crear un reporte
	
	Ti.App.Properties.setString('transporte','particular');
	var pickerUnidades = Ti.UI.createPicker({
	  top:"40%",
	  borderColor:"black",
	  width:"80%",
	  height:"15%",
	  borderRadius:7,
	  backgroundColor:"black"
	});
	
	var unidades = [];
	unidades[0]=Ti.UI.createPickerRow({title:'Selecciona Unidad UAM'});
	unidades[1]=Ti.UI.createPickerRow({title:'Azcapotzalco'});
	unidades[2]=Ti.UI.createPickerRow({title:'Cuajimalpa'});
	unidades[3]=Ti.UI.createPickerRow({title:'Iztapalapa'});
	unidades[4]=Ti.UI.createPickerRow({title:'Lerma'});
	unidades[5]=Ti.UI.createPickerRow({title:'Xochimilco'});
	pickerUnidades.add(unidades);
	pickerUnidades.selectionIndicator = true;
	
	//Obtiene el valor del menu de las unidades.
	pickerUnidades.addEventListener('change',function(e){
		var unidad = pickerUnidades.getSelectedRow(0).title;
		Ti.App.Properties.setString('unidad',unidad);
	});
	
	var pickerEventos = Ti.UI.createPicker({
	  top:"60%",
	  borderColor:"black",
	  width:"80%",
	  height:"15%",
	  borderRadius:7,
	  backgroundColor:"black"
	});
	
	var eventos = [];
	eventos[0]=Ti.UI.createPickerRow({title:'Selecciona un evento'});
	eventos[1]=Ti.UI.createPickerRow({title:'Calle cerrada'});
	eventos[2]=Ti.UI.createPickerRow({title:'Choque'});
	eventos[3]=Ti.UI.createPickerRow({title:'Manifestación'});
	eventos[4]=Ti.UI.createPickerRow({title:'Tráfico pesado'});
	
	pickerEventos.add(eventos);
	pickerEventos.selectionIndicator = true;
	
	//Obtiene el valor del menu de los eventos.
	pickerEventos.addEventListener('change',function(e){
		var evento = pickerEventos.getSelectedRow(0).title;
		Ti.App.Properties.setString('evento',evento);
	});
	
	//Se agregan los componentes en la vista ya definida en el xml y tss.
	$.v_cuerpo.removeAllChildren();
	$.v_cuerpo.add(pickerUnidades);
	$.v_cuerpo.add(pickerEventos);
	$.btn_metro.setBorderColor("white");
	$.btn_particular.setBorderColor("black");
	$.btn_otro.setBorderColor("white");
}

//Esta funcion es muy similar a la funcion de transporte particular, se registran los mismos campos de informacion en el reporte.
function clickOtro(){
	
	var latitude;
	var longitude; 
	//Esta funcion obtiene la ubicacion del usuario, lat y long, esto siempre y caundo la app tenga los permisos de ubicacion
	Titanium.Geolocation.getCurrentPosition(function(e) {
	    if (e.error) {
	       	Ti.API.error('Error: ' + e.error);
	   	} else {
	       	latitude = e.coords.latitude;
	       	longitude = e.coords.longitude;
	       	Ti.App.Properties.setDouble("log",longitude);
	       	Ti.App.Properties.setDouble("lat",latitude);
	       	//Esta funcion transforma lat y long en la direccion como calle, delegacion, etc.. del usuario, esto para poder
	       	//ingresarlo en los reportes.
	       	Titanium.Geolocation.reverseGeocoder(latitude,longitude, function(evt) {
        	var street;
        	var city;
        	var country;
        	if (evt.success) {
            	var places = evt.places;
            	if (places && places.length) {
                	street = places[0].street;
                	city = places[0].city;
                	country = places[0].country_code;
                	var dir = street+" "+city+" "+country;
                	Ti.App.Properties.setString('lugar',dir);
                	console.log(dir);
            	} else {
                	address = "No address found";
            	}
        }

    });                                            
	    }
	});
	
	//********************************************************************
	//Aqui se crean los pequenios menus con las opciones a seleccionar para crear un reporte
	
	Ti.App.Properties.setString('transporte','otro');
	var pickerUnidades = Ti.UI.createPicker({
	  top:"40%",
	  borderColor:"black",
	  width:"80%",
	  height:"15%",
	  borderRadius:7,
	  backgroundColor:"black"
	});
	
	var unidades = [];
	unidades[0]=Ti.UI.createPickerRow({title:'Selecciona Unidad UAM'});
	unidades[1]=Ti.UI.createPickerRow({title:'Azcapotzalco'});
	unidades[2]=Ti.UI.createPickerRow({title:'Cuajimalpa'});
	unidades[3]=Ti.UI.createPickerRow({title:'Iztapalapa'});
	unidades[4]=Ti.UI.createPickerRow({title:'Lerma'});
	unidades[5]=Ti.UI.createPickerRow({title:'Xochimilco'});
	pickerUnidades.add(unidades);
	pickerUnidades.selectionIndicator = true;
	
	//Obtiene el valor del menu de las unidades.
	pickerUnidades.addEventListener('change',function(e){
		var unidad = pickerUnidades.getSelectedRow(0).title;
		Ti.App.Properties.setString('unidad',unidad);
	});
	
	var pickerEventos = Ti.UI.createPicker({
	  top:"60%",
	  borderColor:"black",
	  width:"80%",
	  height:"15%",
	  borderRadius:7,
	  backgroundColor:"black"
	});
	
	var eventos = [];
	eventos[0]=Ti.UI.createPickerRow({title:'Selecciona un evento'});
	eventos[1]=Ti.UI.createPickerRow({title:'Calle cerrada'});
	eventos[2]=Ti.UI.createPickerRow({title:'Choque'});
	eventos[3]=Ti.UI.createPickerRow({title:'Manifestación'});
	eventos[4]=Ti.UI.createPickerRow({title:'Tráfico pesado'});
	eventos[5]=Ti.UI.createPickerRow({title:'Sin unidades en base'});
	
	pickerEventos.add(eventos);
	pickerEventos.selectionIndicator = true;
	
	//Obtiene el valor del menu de los eventos.
	pickerEventos.addEventListener('change',function(e){
		var evento = pickerEventos.getSelectedRow(0).title;
		Ti.App.Properties.setString('evento',evento);
	});
	
	//Se agregan los componentes en la vista ya definida en el xml y tss.
	$.v_cuerpo.removeAllChildren();
	$.v_cuerpo.add(pickerUnidades);
	$.v_cuerpo.add(pickerEventos);
	$.btn_metro.setBorderColor("white");
	$.btn_particular.setBorderColor("white");
	$.btn_otro.setBorderColor("black");
}

//Esta funcion te redirecciona a la ventana de modulos al dar click en el boton cancelar en la ventana de reporte
function clickCancelar(){
	Alloy.createController('modulos').getView().open();
}

//Esta funcion recoge los datos ingresados por el usuario para que crear su reporte dependiendo de las opciones seleccionadas en los datos.
//Esto se realiza por medio de una API que se genero para que realize esta tarea de POST.
function clickReportar(){
	//Alloy.createController('modulos').getView().open();
	var transporte = Ti.App.Properties.getString('transporte');
	var url;
	if(transporte == 'metro'){
		var unidad = Ti.App.Properties.getString('unidad');
		var evento = Ti.App.Properties.getString('evento');
		var linea = Ti.App.Properties.getString('linea');
		var estacion = Ti.App.Properties.getString('estacion');
		url = "https://a81fa40a.ngrok.io/ApiUam/src/public/index.php/AgregarReporteMetro?UnidadUam="+unidad+"&evento="+evento+"&linea="+linea+"&estacion="+estacion;
	};
	if(transporte == 'particular'){
		var unidad = Ti.App.Properties.getString('unidad');
		var evento = Ti.App.Properties.getString('evento');
		var lat = Ti.App.Properties.getDouble('lat');
		var log = Ti.App.Properties.getDouble('log');
		var lugar = Ti.App.Properties.getString('lugar');
		url = "https://a81fa40a.ngrok.io/ApiUam/src/public/index.php/AgregarReporteParticular?UnidadUam="+unidad+"&evento="+evento+"&lugar="+lugar;
		
	};
	if(transporte == 'otro'){
		var unidad = Ti.App.Properties.getString('unidad');
		var evento = Ti.App.Properties.getString('evento');
		var lat = Ti.App.Properties.getDouble('lat');
		var log = Ti.App.Properties.getDouble('log');
		var lugar = Ti.App.Properties.getString('lugar');
		url = "https://a81fa40a.ngrok.io/ApiUam/src/public/index.php/AgregarReporteOtro?UnidadUam="+unidad+"&evento="+evento+"&lugar="+lugar;
			
	};
	//Aqui se conecta con la API
	var client = Ti.Network.createHTTPClient({
	    onload : function(e) {
	        var datos = this.responseText;
	        try {
	        	reporteCreado = JSON.parse(datos);
	        	datos = null;
	        	var toast=Ti.UI.createNotification({});
	        	if(reporteCreado=="OK")
	        	{
	        		toast.setMessage('Su reporte se creo con exito');
					toast.show();
					$.w_reporte.close();
					//Alloy.createController('modulos').getView().open();
	        	}
	        	else
	        	{
	        		toast.setMessage('Hubo un error al crear el reporte');
					toast.show();
	        	}
	            
	        } catch(e_2) {
	        	var a = Ti.UI.createAlertDialog({
					title : 'Ruta Uam',
					ok : 'Aceptar',
					message : 'Verifique su conexión a internet'
				});
				a.show();
	        };
	        
	    },
	    onerror : function(e) {
	        
	        Ti.API.debug(e.error);
	        var a = Titanium.UI.createAlertDialog({
	            title : 'Ruta Uam',
	            ok : 'Aceptar',
	            message : 'Verifique su conexión a internet.'
	        });
	        a.show();
	
	    },
	    timeout : 10000
	});
	
	client.open("POST", url);
	client.send();
	//$.container.close();	
}
