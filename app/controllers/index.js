//Esta funcion abre la ventana iniciarSesion al dar click en el boton de iniciar sesion
//que esta en la ventana index
function openInSesion(e){
	Alloy.createController('iniciarSesion').getView().open();
};

//Esta funcion abre la ventana CrearCuenta al dar click en el boton de registrarse
//que esta en la ventana index
function openRegistro(e) {
	Alloy.createController('CrearCuenta').getView().open();
};

//Esta funcion muestra la ventana index
$.index.open();
