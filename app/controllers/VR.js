//Se conecta con la base de datos de los reportes para recogerlos (actualmente implementado solo para reportes de metro)
//Esto se realiza por medio de una API que se creo para la realizacion de esta tarea GET.
var url = "https://a81fa40a.ngrok.io/ApiUam/src/public/index.php/consultaReportesMetro";
var client = Ti.Network.createHTTPClient({
	onload : function(e) {
		var datos = this.responseText;
		try {
			cuenta = JSON.parse(datos);
			datos = null;
			var reportes = cuenta.length;

			var aux = Ti.UI.createNotification({});
			//aux.setMessage(reportes);
			// aux.show();

			for (var i = 0; i < reportes; i++) {

				var evento = cuenta[i]['evento'];

			};

		} catch(e_2) {
			var a = Ti.UI.createAlertDialog({
				title : 'Ruta Uam',
				ok : 'Aceptar',
				message : 'Verifique su conexión a internet'
			});
			a.show();
		};

	},
	onerror : function(e) {

		Ti.API.debug(e.error);
		var a = Titanium.UI.createAlertDialog({
			title : 'Ruta Uam',
			ok : 'Aceptar',
			message : 'Verifique su conexión a internet.'
		});
		a.show();

	},
	timeout : 10000
});

client.open("GET", url);
client.send();
//*******************************************************************************************
//Aqui se genera un slide menu con algunas opciones en el.
var slidemenu = Titanium.UI.createView({
	width : "45%",
	height : "55%",
	backgroundColor : "white",
	top : "0",
	left : "-1000",

	layout : "vertical"
});
var L_Perfil = Titanium.UI.createLabel({

	text : "MI Perfil",
	color : "black",
	height : "20%",
	width : Titanium.UI.FILL,
	textAlign : "center",
	font : {
		fontSize : 20
	},
	//borderColor :"black"
});
slidemenu.add(L_Perfil);
var L_Inicio = Titanium.UI.createLabel({
	top : 15,
	text : "Inicio",
	color : "black",
	height : "20%",
	width : Titanium.UI.FILL,
	textAlign : "center",
	font : {
		fontSize : 20
	},
	//borderColor :"black"

});
slidemenu.add(L_Inicio);

L_Inicio.addEventListener('click', function(e) {
	$.VR.close();
});
var L_Acerca = Titanium.UI.createLabel({
	top : 15,
	text : "Acerca de",
	color : "black",
	height : "20%",
	width : Titanium.UI.FILL,
	textAlign : "center",
	font : {
		fontSize : 20
	},
	//borderColor :"black"

});
slidemenu.add(L_Acerca);
var L_Ayuda = Titanium.UI.createLabel({
	top : 15,
	text : "Ayuda",
	color : "black",
	height : "20%",
	width : Titanium.UI.FILL,
	textAlign : "center",
	font : {
		fontSize : 20
	},
	//borderColor :"black"

});
slidemenu.add(L_Ayuda);
$.contenidoVR.add(slidemenu);

//Esta funcion realiza el evento de la animacion del slide menu.
$.menuBotonVR.addEventListener('click', function() {

	if (slidemenu.getLeft() < 0) {
		Ti.API.info("Despliego Menu");
		slidemenu.animate({
			left : 0,
			duration : 500
		}, function(e) {
			slidemenu.setLeft(0);
		});
	} else {
		Ti.API.info("Cierro Menu");
		slidemenu.animate({
			left : -1000,
			duration : 500
		}, function(e) {
			slidemenu.setLeft(-1000);
		});
	}

});

//************** Time Line**************
//Aqui se generan las vistas para poder visualizar los reportes (actualmente solo metro) creados de los usuarios.

function time_line(cuenta) {
	var arregloDatos = [];
	for (var i = 0; i < cuenta.length; i++) {
		var row = Titanium.UI.createTableViewRow({
			height : '100'
		});
		/*	id:"1",
		 medio:'particular',
		 evento : "Calle cerrada",
		 ubicacion:"Eje 6",
		 contador : "3",
		 contador_DL : "0",
		 */
		//la variable cuenta contiene la informacion devuelta por la API (pedir reportes) 
		var esteid = cuenta[i]['id'];
		var esteEvento = cuenta[i]['evento'];
		var esteUbi = cuenta[i]['lugar'];
		var contaLikes = cuenta[i]['positivo'];
		var contaDLikes = cuenta[i]['negativo'];
		var Event_view = Titanium.UI.createView({
			height : 'auto',
			top : 5,
			layout : 'horizontal',
			width : "95%",
			borderColor : 'black',
			borderRadius : 5,
			backgroundColor : '#F6F9FC'
		});
		var info = Titanium.UI.createView({
			height : Titanium.UI.FILL,
			width : '55%',
			left : 5,
			top : 0,
			layout : 'vertical',

			backgroundColor : '#F6F9FC'
		});

		var Evento = Titanium.UI.createLabel({
			height : "70%",
			width : Titanium.UI.FILL,
			text : esteEvento,
			color : "black",
			textAlign : 'left',
			left : "2",
			font : {
				fontFamily : 'Trebuchet MS',
				fontSize : 20,
				fontWeight : 'bold'
			}

		});
		info.add(Evento);
		var ubicacion = Titanium.UI.createLabel({
			height : "30%",
			width : Titanium.UI.FILL,
			text : esteUbi,
			color : '#010100',
			top : 1,
			textAlign : 'left',
			font : {
				fontFamily : 'Trebuchet MS',
				fontSize : 20
			}
		});

		info.add(ubicacion);
		//win_P.add(Event_view);

		Event_view.add(info);
		var view_Like = Titanium.UI.createView({
			height : Titanium.UI.FILL,
			width : "18%",
			layout : "vertical"

		});
		var like = Titanium.UI.createImageView({
			image : "/images/like.png",
			top : 8,
			left : 6,
			height : '60%',

		});
		var contL = Titanium.UI.createLabel({
			height : "30%",
			width : Titanium.UI.FILL,
			text : contaLikes,
			color : '#010100',
			top : 1,
			textAlign : 'center',
			color : "green",
			font : {
				fontFamily : 'Trebuchet MS',
				fontSize : 14
			}

		});
		//Se agregan las vista para el icono de like generadas en la vista correspondiente, una vista para cada reporte obtenido de la API.
		view_Like.add(like);
		view_Like.add(contL);
		view_Like.addEventListener('click', function(e) {
var auxL = parseInt(contaLikes,10);
auxL=auxL+1;
			var url2 = "https://a81fa40a.ngrok.io/ApiUam/src/public/index.php/ActulizarPositivoMetro?id=" + esteid + "&puntos=" + auxL;

			var client2 = Ti.Network.createHTTPClient({
				onload : function(e) {
					var datos = this.responseText;
					try {
						cuentaCreada = JSON.parse(datos);
						datos = null;
						var toast = Ti.UI.createNotification({});
						if (cuentaCreada == "OK") {

							toast.setMessage('like');
							toast.show();
							} else {
							toast.setMessage('error');
							toast.show();
						}

					} catch(e_2) {
						var a = Ti.UI.createAlertDialog({
							title : 'Ruta UAM',
							ok : 'Aceptar',
							message : url2
						});
						a.show();
					};

				},
				onerror : function(e) {

					Ti.API.debug(e.error);
					var a = Titanium.UI.createAlertDialog({
						title : 'SmartCDMX',
						ok : 'Aceptar',
						message : url2
					});
					a.show();

				},
				timeout : 10000
			});

			client2.open("POST", url2);
			client2.send();
		});
		Event_view.add(view_Like);
		var view_DLike = Titanium.UI.createView({
			height : Titanium.UI.FILL,
			width : "18%",
			layout : "vertical"

		});

		var Dlike = Titanium.UI.createImageView({
			image : "/images/disLike.png",
			top : 8,
			left : 6,
			height : '60%',

		});
		var contDL = Titanium.UI.createLabel({
			height : "30%",
			width : Titanium.UI.FILL,
			text : contaDLikes,

			top : 1,
			textAlign : 'center',
			color : "red",
			font : {
				fontFamily : 'Trebuchet MS',
				fontSize : 14
			}

		});
		//Se agregan las vista para el icono de dislike generadas en la vista correspondiente, una vista para cada reporte obtenido de la API.
		view_DLike.add(Dlike);
		view_DLike.add(contDL);
		Event_view.add(view_DLike);
		view_DLike.addEventListener('click', function(e) {
			alert(esteid + "," + contaDLikes);
		});
		row.add(Event_view);
		arregloDatos.push(row);
	};
	return arregloDatos;
};
var tabla_events = Titanium.UI.createTableView({

	borderWidth : 2,
	//borderColor : '#5AA8E2',
	//backgroundColor : 'blue',
	height : Titanium.UI.FILL,
	top : 30,
	left : 5,
	right : 5
});
//Se agregan las vistas generadas para cada reporte a la vista ya definida en el xml y tss.
tabla_events.setData(time_line(cuenta));
$.timeline.add(tabla_events);

//Se crea e inserta pequenio menu de opciones para poder filtrar los reportes (funcionalidad no alcanzada en esta etapa). 
var pickerTransporte = Ti.UI.createPicker({
	//top:"5%",
	borderColor : "black",
	width : "80%",
	height : "70%",
	borderRadius : 7,
	backgroundColor : "black"
});

var transporte = [];
transporte[0] = Ti.UI.createPickerRow({
	title : 'Seleccionar transporte'
});
transporte[1] = Ti.UI.createPickerRow({
	title : 'Metro'
});
transporte[2] = Ti.UI.createPickerRow({
	title : 'Particular'
});
transporte[3] = Ti.UI.createPickerRow({
	title : 'Otro'
});

pickerTransporte.add(transporte);
pickerTransporte.selectionIndicator = true;
$.Filtro.add(pickerTransporte);
