//En estas primeras lineas se crea el slide menu para agregarse a la ventana de modulos
//Este menu contiene algunas opciones como: perfil, inicio, acerca de y salir.
var slidemenu = Titanium.UI.createView({
	width : "45%",
	height : "55%",
	backgroundColor :"white",
	top:0,
	left : "-1000",

	layout: "vertical"
});
var L_Perfil = Titanium.UI.createLabel({
	
	text : "MI Perfil",
	color:"black",
	height:"20%",
	width : Titanium.UI.FILL,
	textAlign : "center",
	font : {
		fontSize : 20
	},
	//borderColor :"black"
});
slidemenu.add(L_Perfil);
var L_Inicio = Titanium.UI.createLabel({
	top:15,
	text : "Inicio",
	color:"black",
	height:"20%",
	width : Titanium.UI.FILL,
	textAlign : "center",
	font : {
		fontSize : 20
	},
	//borderColor :"black"
	
});
slidemenu.add(L_Inicio);
 L_Inicio.addEventListener('click',function(e){
 	
 });

var L_Acerca = Titanium.UI.createLabel({
	top:15,
	text : "Acerca de",
	color:"black",
	height:"20%",
	width : Titanium.UI.FILL,
	textAlign : "center",
	font : {
		fontSize : 20
	},
	//borderColor :"black"
	
});
slidemenu.add(L_Acerca);
var L_Salir = Titanium.UI.createLabel({
	top:15,
	text : "Salir",
	color:"black",
	height:"20%",
	width : Titanium.UI.FILL,
	textAlign : "center",
	font : {
		fontSize : 20
	},
	//borderColor :"black"
	
});
slidemenu.add(L_Salir);

 L_Salir.addEventListener('click',function(e){
 	$.modulos.close();
 	Alloy.createController('index').getView().open();
 });
$.contenido.add(slidemenu);

//Evento para la realizacion de la animacion del slide menu.
$.menuBoton.addEventListener('click',function(){
	
	if(slidemenu.getLeft() <0){
		Ti.API.info("Despliego Menu");
	slidemenu.animate({
        left: 0,
        duration: 500
   },function(e){
   	slidemenu.setLeft(0);
   }
    
    );
	}else{
		Ti.API.info("Cierro Menu");
		slidemenu.animate({
        left: -1000,
        duration: 500
    },function(e){
   	slidemenu.setLeft(-1000);
   });
	}

});

//Funcion que te redirecciona a la ventana Reporte al dar click en el boton reportar
function openReportar(){
	Alloy.createController('Reporte').getView().open();
}

//Funcion que te redirecciona a la ventana miRuta al dar click en el boton miRuta
function openMR(){
	Alloy.createController('miRuta').getView().open();
}

//Funcion que te redirecciona a la ventana VR al dar click en el boton ver reportes
function openVR(){
	Alloy.createController('VR').getView().open();
}