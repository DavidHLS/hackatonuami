// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

//Esta funcion obtiene la informacion proporcionada por el usuario para crear su registro en una base de datos,
//esto por medio de una API creada para realizar esta tarea.
//Si el registro tiene exito se manda un mensaje de exito de lo contrario se manda un mensaje de error.
//Si ocurre un error al conectarse con la API se manda una mensaje de error.
function doCrearCuenta(e){
	
	var matricula=$.TA_matricula.getValue();
	var nombre=$.TA_nombre.getValue();
	var pass=$.TF_password.getValue();
	var url = "https://a81fa40a.ngrok.io/ApiUam/src/public/index.php/AgregarAlumno?matricula="+matricula+"&nombre="+nombre+"&password="+pass;

	var client = Ti.Network.createHTTPClient({
	    onload : function(e) {
	        var datos = this.responseText;
	        try {
	        	cuentaCreada = JSON.parse(datos);
	        	datos = null;
	        	var toast=Ti.UI.createNotification({});
	        	if(cuentaCreada=="OK")
	        	{
	        		toast.setMessage('Su cuenta se creo con exito');
					toast.show();
					Alloy.createController('index').getView().open();
	        	}
	        	else
	        	{
	        		toast.setMessage('Hubo un error al crear la cuenta');
					toast.show();
	        	}
	            
	        } catch(e_2) {
	        	var a = Ti.UI.createAlertDialog({
					title : 'Ruta Uam',
					ok : 'Aceptar',
					message : 'Verifique su conexión a internet'
				});
				a.show();
	        };
	        
	    },
	    onerror : function(e) {
	        
	        Ti.API.debug(e.error);
	        var a = Titanium.UI.createAlertDialog({
	            title : 'SmartCDMX',
	            ok : 'Aceptar',
	            message : 'Verifique su conexión a internet.'
	        });
	        a.show();
	
	    },
	    timeout : 10000
	});
	
	client.open("POST", url);
	client.send();
	$.container.close();
	//Cierra la ventana llamada container	
}
