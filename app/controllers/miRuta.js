// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

//Esta funcion muestra un pequenio menu desplegable al dar click en el icono de metro con las unidades de Uam, para que el usuario seleccione su unidad de destino.
//Y poderle mostrar reportes con ese destino que le pueden ser de utilidad en su transcurso a la unidad.
function clickMetro(){
	var pickerUnidades = Ti.UI.createPicker({
	  borderColor:"black",
	  width:"80%",
	  height:"20%",
	  borderRadius:7,
	  backgroundColor:"black"
	});
	
	var unidades = [];
	unidades[0]=Ti.UI.createPickerRow({title:'Selecciona Unidad UAM'});
	unidades[1]=Ti.UI.createPickerRow({title:'Azcapotzalco'});
	unidades[2]=Ti.UI.createPickerRow({title:'Cuajimalpa'});
	unidades[3]=Ti.UI.createPickerRow({title:'Iztapalapa'});
	unidades[4]=Ti.UI.createPickerRow({title:'Lerma'});
	unidades[5]=Ti.UI.createPickerRow({title:'Xochimilco'});
	pickerUnidades.add(unidades);
	pickerUnidades.selectionIndicator = true;
	
	//Se agregan los componentes a la vista ya definida en el xml y tss.
	$.viewCuerpo.removeAllChildren();
	$.viewCuerpo.add(pickerUnidades);
	$.btMetro.setBorderColor("black");
	$.btParticular.setBorderColor("white");
	$.btOtro.setBorderColor("white");

	pickerUnidades.setSelectedRow(0, 0, false);
}

//La funcionalidad de esta funcion es muy parecida a la anterior, solo que esta al dar click en el icono de particular. 
function clickParticular(){
	var pickerUnidades = Ti.UI.createPicker({
	  borderColor:"black",
	  width:"80%",
	  height:"20%",
	  borderRadius:7,
	  backgroundColor:"black"
	});
	
	var unidades = [];
	unidades[0]=Ti.UI.createPickerRow({title:'Selecciona Unidad UAM'});
	unidades[1]=Ti.UI.createPickerRow({title:'Azcapotzalco'});
	unidades[2]=Ti.UI.createPickerRow({title:'Cuajimalpa'});
	unidades[3]=Ti.UI.createPickerRow({title:'Iztapalapa'});
	unidades[4]=Ti.UI.createPickerRow({title:'Lerma'});
	unidades[5]=Ti.UI.createPickerRow({title:'Xochimilco'});
	pickerUnidades.add(unidades);
	pickerUnidades.selectionIndicator = true;
	
	//Se agregan los componentes a la vista ya definida en el xml y tss.
	$.viewCuerpo.removeAllChildren();
	$.viewCuerpo.add(pickerUnidades);
	$.btMetro.setBorderColor("white");
	$.btParticular.setBorderColor("black");
	$.btOtro.setBorderColor("white");

	pickerUnidades.setSelectedRow(0, 0, false);
}

//La funcionalidad de esta funcion es muy parecida a la anterior, solo que esta al dar click en el icono de otro.
function clickOtro(){
	var pickerUnidades = Ti.UI.createPicker({
	  borderColor:"black",
	  width:"80%",
	  height:"20%",
	  borderRadius:7,
	  backgroundColor:"black"
	});
	
	var unidades = [];
	unidades[0]=Ti.UI.createPickerRow({title:'Selecciona Unidad UAM'});
	unidades[1]=Ti.UI.createPickerRow({title:'Azcapotzalco'});
	unidades[2]=Ti.UI.createPickerRow({title:'Cuajimalpa'});
	unidades[3]=Ti.UI.createPickerRow({title:'Iztapalapa'});
	unidades[4]=Ti.UI.createPickerRow({title:'Lerma'});
	unidades[5]=Ti.UI.createPickerRow({title:'Xochimilco'});
	pickerUnidades.add(unidades);
	pickerUnidades.selectionIndicator = true;
	
	//Se agregan los componentes a la vista ya definida en el xml y tss.
	$.viewCuerpo.removeAllChildren();
	$.viewCuerpo.add(pickerUnidades);
	$.btMetro.setBorderColor("white");
	$.btParticular.setBorderColor("white");
	$.btOtro.setBorderColor("black");

	pickerUnidades.setSelectedRow(0, 0, false);
}

//Funcion que te redirecciona a la ventana modulos al dar click en el boton cancelar de la ventana miRuta
function clickCancelar(){
	Alloy.createController('modulos').getView().open();
}

//Funcion que te redirecciona a la ventana VR al dar click en el boton crear de la ventana miRuta
function clickCrear(){
	Alloy.createController('VR').getView().open();
}
