// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

//Esta funcion abre la ventana llamada index al dar click en el boton de cancelar de la ventana iniciarSesion
function openPrincipal(e){
	Alloy.createController('index').getView().open();
};

//Esta funcion valida la matricula ingresada por el usuario con las ya registradas con la base de datos ya que estas son unicas
//para cada usuario, esto por medio de una API creada para realizar esta tarea de GET. 
//Se lanza un mensaje de exito o error segun sea el caso de la conexion con la API.
function validarCuenta(e){
	var matricula=$.txtMatricula.getValue();
	var password=$.txtContrasenia.getValue();
	var url = "https://a81fa40a.ngrok.io/ApiUam/src/public/index.php/consultaAlumno?matricula="+matricula;
	var client = Ti.Network.createHTTPClient({
	    onload : function(e) {
	        var datos = this.responseText;
	        try {
	        	cuenta = JSON.parse(datos);
	        	datos = null;
	        	var mat=cuenta.matricula;
	        	var pass=cuenta.password;
	        	var toast=Ti.UI.createNotification({});
	        	
	        	if(matricula!=null)
	        	{
	        		if((matricula==mat)&&(password==pass))
		        	{
		        		toast.setMessage('Inicio exitoso');
						toast.show();
						Alloy.createController('modulos').getView().open();
		        	}
		        	else
		        	{
		        		toast.setMessage('contraseña invalida');
						toast.show();
		        	}	
	        	}
	        	else
	        	{
	        		toast.setMessage('el usuario no existe');
					toast.show();
	        	}
	            
	        } catch(e_2) {
	        	var a = Ti.UI.createAlertDialog({
					title : 'Ruta Uam',
					ok : 'Aceptar',
					message : 'Verifique su conexión a internet'
				});
				a.show();
	        };
	        
	    },
	    onerror : function(e) {
	        
	        Ti.API.debug(e.error);
	        var a = Titanium.UI.createAlertDialog({
	            title : 'SmartCDMX',
	            ok : 'Aceptar',
	            message : 'Verifique su conexión a internet.'
	        });
	        a.show();
	
	    },
	    timeout : 10000
	});
	
	client.open("GET", url);
	client.send();
};